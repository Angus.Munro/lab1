package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random; 

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    boolean continuePlay = true;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        while(continuePlay){
            System.out.println("Let's play round " + roundCounter);
            String humanMove = readInput("Your choice (Rock/Paper/Scissors)?");
            while(validInput(humanMove) == false) {
                System.out.println("I do not understand " + humanMove + ". Could you try again? ");
                humanMove = readInput("Your choice (Rock/Paper/Scissors)?");
            }
            // Computer makes a move
            Random random = new Random();
            int randomNumber = random.nextInt(3);
            String computerMove = rpsChoices.get(randomNumber);
            /* 
            if (randomNumber == 0) {
                computerMove = "rock";
            } else if (randomNumber == 1) {
                computerMove = "paper";
            } else {
                computerMove = "scissors";
            }*/
            System.out.printf("Human chose %s, computer chose %s. ", humanMove, computerMove);
            if (humanMove.equals(computerMove)){
                System.out.printf("It's a tie!\n");
            } else if (isWinner(humanMove, computerMove)) {
                System.out.printf("Human wins!\n");
                humanScore++;
            } else {
                System.out.printf("Computer wins!\n");
                computerScore++;
            }
            roundCounter++;
            System.out.printf("Score: human %d, computer %d\n", humanScore, computerScore);
            continuePlay = (readInput("Do you wish to continue playing? (y/n)?").equals("y"));
        }
        System.out.println("Bye bye :)");
    }
    static boolean isWinner(String humanMove, String computerMove) {
        if (humanMove.equals("rock")) {
            return computerMove.equals("scissors");
        } else if (humanMove.equals("paper")) {
            return computerMove.equals("rock");
        } else {
            return computerMove.equals("paper");
        }
    }
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput.toLowerCase();
    }
    public boolean validInput(String text) {
        return rpsChoices.contains(text);
    }

}
